<?php 
/**
 * JsTree class file
 * 
 * @author Dmitry
 *
 */
/**
 * JsTree displays a tree view of hierarchical data.
 *
 * It encapsulates the excellent tree view plugin for jQuery
 * ({@link http://www.jstree.com/}).
 *
 * To use JsTree, simply sets {@link data} to the data that you want
 * to present and you are there.
 *
 * JsTree also supports dynamic data loading via AJAX. To do so, set
 * {@link url} to be the URL that can serve the tree view data upon request.
 *
 * @author Dmitry
 * @version $Id$
 * @package application.extensions
 * @since 1.0
 */
class JsTree extends CWidget
{
	public $config = array();
	
	public $init = true;
	
	public $operations;
	
	public function init()
	{
		$assets = dirname(__FILE__).'/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		$cs = Yii::app()->clientScript;
		//$cs->registerScriptFile($baseUrl . '/jquery.jstree.js');
		$cs->registerScriptFile($baseUrl . '/jquery.jstree.min.js');
		$cs->registerScriptFile($baseUrl . '/_lib/jquery.cookie.js');
		$cs->registerScriptFile($baseUrl . '/_lib/jquery.hotkeys.js');
		if ($this->init) {
			$cs->registerScript('Yii.JsTree.tree#'.$this->getId(), 'jQuery("#' . $this->getId() . '").jstree(' . $this->_getConfigObject() . ')' . $this->_getOperations() . ';');
		}
		echo CHtml::openTag('div', array('id' => $this->getId()));
	}
	
	public function run()
	{
		echo CHtml::closeTag('div');
	}
	
	protected function _getConfigObject()
	{
		$this->config['core']['strings'] = array(
			'loading'  			=> Yii::t('jstree.main', 'Loading ...'),
			'new_node' 			=> Yii::t('jstree.main', 'New category'),
			'multiple_selection'=> Yii::t('jstree.main', 'Multiple selection')
		);
		return CJavaScript::encode($this->config);	
	}
	
	protected function _getOperations()
	{
		$operations = '';
		if(!empty($this->operations) && is_array($this->operations)) {
			foreach($this->operations as $name=>$operation) {
				$operations .= ".bind('{$name}.jstree', " . CJavaScript::encode($operation) . ")";
			}
		}
		return $operations;		
	}
	
}
