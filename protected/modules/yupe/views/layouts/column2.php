<?php $this->beginContent($this->yupe->getBackendLayoutAlias("main")); ?>
  <div class="row-fluid">
    <div class="span9">
        <?php
        if (count($this->breadcrumbs))
            $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                 'homeLink' => CHtml::link(Yii::t('YupeModule.yupe', 'Главная'), array('/yupe/backend/index')),
                 'links'    => $this->breadcrumbs,
            ));
        ?><!-- breadcrumbs -->
        <?php $this->widget('YFlashMessages');?>
        <div id="content">
            <?php echo $content; ?>
        </div>
        <!-- content -->
    </div>
    <div class="span3" style="margin-top: 18px;">
        <?php if (count($this->menu)): ?>
            <div class="well" style="padding: 8px 0;">
                <?php $this->widget('bootstrap.widgets.TbMenu', array(
                    'type' => 'list',
                    'items' => $this->yupe->getSubMenu($this->menu),
                )); ?>
            </div>
        <?php endif; ?>

        <div class="well" style="padding: 8px;"><?php $this->widget('YModuleInfo'); ?></div>
        <div style="display:block" class="well">
            <div id="category_tree_popup">
                <?php
                $this->widget('application.extensions.jstree.JsTree', array(
                        'id' => 'category_tree',
                        'config' => array(
                            'core' => array(
                                'initially_open' => array("node_root"),
                                'load_open' => false
                            ),
                            'plugins' => array(
                                'themes', 'json_data',  'ui', 'hotkeys'
                            ),
                            'ui' => array(
                                'select_limit' => 0
                            ),
                            "themes" => array(
                                "theme" => "classic",
                                "icons" => false
                            ),
                            "json_data" => array(
                                "ajax" => array(
                                    "url" => $this->createAbsoluteUrl('/menu/menuitem/getpagetree'),
                                )
                            )
                        ),
                        'operations' => array(
                            'loaded.jstree' => 'js:function(e, data) {
                        $(\'.menu-item\').click(function() {
                            window.location = "/page/default/update/id/" + $(this).data("id");
                        });
                        $(\'.root\').click(function() {
                            window.location = "/page/default/index";
                        });
					}',

                        ),

                    )
                );
                ?>
            </div>
        </div>
    </div>
  </div>
<?php $this->endContent(); ?>
