<?php
$this->breadcrumbs = array(
    Yii::t('YupeModule.yupe', 'Юпи!') => array('settings'),
    Yii::t('YupeModule.yupe','Помощь')
);
?>

<h1><?php echo Yii::t('YupeModule.yupe', 'О Webcenter'); ?></h1>

<p> <?php echo Yii::t('YupeModule.yupe','У каждого большого проекта должна быть страничка "О проекте", у нас она именно здесь =)'); ?></p>

<br/>

<p>
    <?php echo Yii::t('YupeModule.yupe','Вы используете Yii версии'); ?>
    <small class="label label-info" title="<?php echo Yii::getVersion(); ?>"><?php echo Yii::getVersion(); ?></small>,
    <?php echo CHtml::encode(Yii::app()->name); ?>
    <?php echo Yii::t('YupeModule.yupe', 'версии'); ?> <small class="label label-info" title="<?php echo $this->yupe->version; ?>"><?php echo $this->yupe->version; ?></small>,
    <?php echo Yii::t('YupeModule.yupe', 'php версии'); ?>
    <small class="label label-info" title="<?php echo phpversion(); ?>"><?php echo phpversion(); ?></small>
</p>

<br/>

<div class="alert">
    <p>
        <?php echo Yii::t('YupeModule.yupe', ' Юпи! разрабатывается и поддерживается командой энтузиастов, Вы можете использовать Юпи! и любую его часть <b>совершенно бесплатно</b>'); ?>
    </p>
    <?php echo CHtml::link(Yii::t('YupeModule.yupe', 'А вот здесь мы принимаем благодарности =)'), 'http://yupe.ru/pages/help?form=help', array('target' => '_blank')); ?>
    <p><p><b>
        <?php echo Yii::t('YupeModule.yupe', 'По вопросам разработки Вы всегда можете <a href="http://webcenter.pro/contacts#feedback" target="_blank">написать нам</a> (<a href="http://webcenter.pro/contacts#feedback" target="_blank">http://webcenter.pro/contacts#feedback</a>)'); ?>
    </b></p></p>
</div>


<br />

<p><b><?php echo Yii::t('YupeModule.yupe', 'Полезные ресурсы:');?></b></p>


<?php echo CHtml::link(Yii::t('YupeModule.yupe', 'Официальный сайт Webcenter', array('target' => '_blank')), 'http://webcenter.pro/?form=help'); ?> - <?php echo Yii::t('YupeModule.yupe', 'заходите чаще!'); ?>

<div class="alert">
    <?php echo Yii::t('YupeModule.yupe', 'Напишите нам на <a href="mailto:company@webcenter.pro">company@webcenter.pro</a> или через форму {link}',array('{link}' => CHtml::link('обратной связи','http://webcenter.pro/contacts#feedback',array('target' => '_blank')))); ?>  - <?php echo Yii::t('YupeModule.yupe', 'принимаем всякого рода коммерческие и любые предложения =)'); ?>
</div>
