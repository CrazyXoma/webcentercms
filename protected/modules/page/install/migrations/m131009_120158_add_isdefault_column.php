<?php

class m131009_120158_add_isdefault_column extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{page_page}}', 'is_default', 'boolean default false');
	}

	public function down()
	{
        $this->dropColumn('{{page_page}}', 'is_default');
	}

}