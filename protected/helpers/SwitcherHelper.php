<?php
class SwitcherHelper
{
    private static $_isFrontend = false;

    public static function getIsFrontend()
    {
        return self::$_isFrontend;
    }

    public static function setIsFrontend( $status )
    {
        self::$_isFrontend = $status;
    }
}
    
?>
