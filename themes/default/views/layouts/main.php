<?php
/**
 * Шаблон для layout/main:
 *
 * @category YupeLayout
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
?>
<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
    <meta charset="UTF-8"/>
    <meta name="keywords" content="<?php echo $this->keywords; ?>"/>
    <meta name="description" content="<?php echo $this->description; ?>"/>
    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/favicon.ico"/>

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/web/css/screen.css"
          media="screen, projection"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/web/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/web/css/main.css"/>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<?php //$this->widget('application.modules.yupe.widgets.YAdminPanel'); ?>
<div class="container" id="page">
    <div id="header">
        <div id="logo">
            <a href="<?php echo CHtml::normalizeUrl(array("/site/index")) ?>">
                <?php echo CHtml::image(Yii::app()->baseUrl . '/web/images/wc_logo_100.png'); ?>
            </a>
            <div>
                <?php echo $this->description; ?>
            </div>
        </div>
    </div>
    <!-- header -->
    <?php
    $this->widget(
        'application.modules.menu.widgets.MenuWidget', array(
            'name' => 'top-menu',
            'params' => array(
                'hideEmptyItems' => true
            ),
            'layoutParams' => array(
                'htmlOptions' => array(
                    'class' => 'jqueryslidemenu',
                    'id' => 'myslidemenu',
                )
            ),
        )
    ); ?>
    <?php $this->widget('application.modules.yupe.extensions.jquerySlideMenu.JquerySlideMenuWidget'); ?>
    <!-- mainmenu -->
    <?php $this->widget('zii.widgets.CBreadcrumbs', array('links' => $this->breadcrumbs)); ?>
    <!-- breadcrumbs -->
    <?php $this->widget('YFlashMessages'); ?>

    <div class="container">
        <div class="span-19">
            <div id="content">
                <?php echo $content; ?>
            </div>
            <!-- content -->
        </div>
        <div class="span-5 last">
            <div id="sidebar">
                <?php $this->widget('application.modules.feedback.widgets.FaqWidget', array('cacheTime' => 0)); ?>
                <?php $this->widget('application.modules.user.widgets.LastLoginUsersWidget', array('cacheTime' => 0)); ?>
            </div>
            <!-- sidebar -->
        </div>
    </div>

    <div id="footer">
        Copyright &copy; 2009-<?php echo date('Y'); ?> <a href="<?php echo Yii::app()->createUrl('/blog/rss/feed/');?>"><img src="<?php echo Yii::app()->theme->baseUrl?>/web/images/rss.png" alt="Подпишитесь на обновления" title="Подпишитесь на обновления"></a>
        <br/><?php echo $this->yupe->poweredBy(); ?>
        <small class="label label-info"><?php echo $this->yupe->getVersion(); ?></small>
        <br/>
        <a href="http://webcenter.pro?from=yupe-main-page" target="_blank"><?php echo Yii::t('YupeModule.yupe', 'Разработка и поддержка'); ?></a> - <a href="http://webcenter.pro/?from=yupe-main-page" target="_blank">Webcenter.pro</a>
        <br/>
        <?php echo Yii::powered(); ?>

        <?php $this->widget('YPerformanceStatistic'); ?>
    </div>
    <!-- footer -->
</div>
<!-- page -->
<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", array("code" => "STAT", "silent" => true)); ?>
</body>
</html>