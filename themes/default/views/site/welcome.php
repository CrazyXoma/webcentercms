<?php if(is_null($default)) : ?>
<h1>Поздравляем!</h1>
<p>Вы установили <a href="http://webcenter.pro?from=mainpage" target='_blank'>Webcenter CMS</a>! Надеемся у Вас не возникло сложностей !?</p>
<div class='alert alert-warning'>
    <p><b>По вопросам разработки и поддержки - <a href='http://webcenter.pro/contacts#feedback' target='_blank'>напишите нам</a> !</b></p>
</div>
<?php if (YII_DEBUG === true) : ?>
<p class='alert alert-info'>
    Если вы замечаете "замедление" в работе сайта - не волнуйтесь, в большинстве случаев это связано с тем, что включён YII_DEBUG режим, при его отключении скорость будет на порядок выше. Но не стоит волноваться, ведь при переносе на реальный сервер YII_DEBUG отключается автоматически.
</p>
<?php endif; ?>

<p> Начните доработку Вашего сайта с правки <code>SiteController</code> (/protected/controller/SiteController)</p>
<p> Для управления сайтом, пожалуйста, перейдите в <?php echo CHtml::link('панель управления', array('/yupe/backend/')); ?></p>
<?php else: ?>
    <?=$default->body?>
<?php endif;?>
